import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;


public class Main {

	public static void main(String[] args){
		
		// abertura do arquivo dataset.csv
		File datasetCSV = new File("./dataset.CSV");
		
		try {
			//leitor do arquivo datasetCSV
			Scanner readerCSV = new Scanner(datasetCSV);
			
			try { 
				//cria/sobrescreve aquivo samuelriosdasilva.txt com os dados requeridos
				FileWriter arq = new FileWriter("./samuelRiosDaSilva.txt");
				PrintWriter gravarArq = new PrintWriter(arq);

				readerCSV.nextLine();

				while(readerCSV.hasNext()) {
					
					String[] arrayData = readerCSV.nextLine().split(";");
					
					if(arrayData.length == 4) {
						float peso = Float.parseFloat(arrayData[2].replaceAll(",","."));
						float altura = Float.parseFloat(arrayData[3].replaceAll(",","."));
						
						float imc = CalculaIMC.calcula(peso, altura);
						
						DecimalFormat decimalformat = new DecimalFormat("0.00");

						String printData = arrayData[0].trim().toUpperCase() 
								+ " " 
								+ arrayData[1].trim().replaceAll("\\s+"," ").toUpperCase()
								+ " "
								+ decimalformat.format(imc).replace(".",",");

						gravarArq.println(printData);
						System.out.println(printData);

					} else {
						String printData = arrayData[0].trim().toUpperCase() 
								+ " " 
								+ arrayData[1].trim().replaceAll("\\s+"," ").toUpperCase()
								+ " "
								+ "- dados insuficientes";

						gravarArq.println(printData);
						System.out.println(printData);
					}
				}
				gravarArq.close();
				readerCSV.close();
			} catch (IOException ioe) { 
				System.out.printf("Erro: Arquivo n�o encontrado!\n");
			}
		} catch (FileNotFoundException e) {
			System.out.printf("Erro: Arquivo n�o encontrado!\n");
		}
		
	}

}
